(function () {
   // тут будем хранить интервал
   let timer;

   // кнопки на экране
   const step1 = document.getElementById('autoStep1');
   const step2 = document.getElementById('autoStep2');
   const stop = document.getElementById('autoStop');

   step1.addEventListener('click', () => {
      // чистим интервал, возможно раньше он был запущен
      clearInterval(timer);
      // сразу делаем шаг
      nextStep();
      // устанавливаем новый интервал со скоростью 1 (т.е. 1000ms / 1 = 1000ms или 1 шаг в сек)
      timer = createTimer(1);
   });

   step2.addEventListener('click', () => {
      clearInterval(timer);
      nextStep();
      timer = createTimer(2);  // 1000ms / 2 = 500ms или 2 шага в сек
   });

   stop.addEventListener('click', () => {
      clearInterval(timer);
   });

   function createTimer(speed) {
      return setInterval(nextStep, 1000 / speed);
   }
})();
