function Matrix(n, m) {
   this.state = [];

   this.getCurrent = () => {
      const lastElem = this.state.length - 1;
      return this.state[lastElem] || [];
   };

   this.init = (n, m) => {
      const matrix = [];

      for (let i = 0; i < n; i++) {
         const row = [];
         for (let j = 0; j < m; j++) {
            const elem = sup.getWithProbability(30);
            row.push(elem);
         }
         matrix.push(row);
      }
      this.state.push(matrix);
   };
   this.init(n, m);

   this.nextState = () => {
      const currentMatrix = this.getCurrent();
      const newMatrix = sup.clone(currentMatrix);

      for (let i = 0; i < newMatrix.length; i++) {
         const row = newMatrix[i];

         for (let j = 0; j < row.length; j++) {
            const point = [i, j];
            const cell = row[j];
            const sellAround = Cell.getCountCellsAround(point, currentMatrix);

            // изменяем значение клетки на нужный
            row[j] = Cell.getStatus(cell, sellAround);
         }
      }
      return newMatrix;
   };

   this.render = (matrixDom) => {
      const currentMatrix = this.getCurrent();

      matrixDom.innerHTML = '';

      for (let i = 0; i < currentMatrix.length; i++) {
         const row = document.createElement('div');
         const line = currentMatrix[i];

         row.className = "matrixRow";
         matrixDom.append(row);

         for (let i = 0; i < line.length; i++) {
            const cell = document.createElement('div');

            if (line[i] === 1) cell.className = "cell";

            row.append(cell);
         }
      }
   };

   this.save = (arr) => {
      this.state.push(arr);
   };

   this.prevStep = () => {
       if (this.state.length === 1) {
           console.warn(`Вы на последнем доступном шаге`);
           return this.getCurrent();
       }

      // удаляем последнюю
      this.state.splice(-1, 1);

      return this.getCurrent();
   };
}
