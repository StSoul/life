function nextStep() {
   const newMatrix = matrix.nextState();

   matrix.save(newMatrix);
   matrix.render(dom);

   setStepInfo();
}

function backStep() {
   matrix.prevStep();
   matrix.render(dom);

   setStepInfo();
}

function setStepInfo() {
   stepInfo.innerText = matrix.state.length;
}

function saveGame() {
   const localData = localStorage.getItem('save');
   const allGames = JSON.parse(localData);

   // хранить массив в локальном хранилище нельзя, только строки
   // JSON.stringify преврящает массив matrix.state в JSON (строковый тип данных)
   // matrix.getCurrent() - получить текущей массив матрици

   const saveName = new Date().toLocaleString('ru');
   const data = {
      ...allGames,
      [saveName]: {
         step: matrix.state.length, // текущий шаг
         state: matrix.getCurrent(), // матрица
      }
   };

   // сохраняем данные под ключем "save"
   localStorage.setItem('save', JSON.stringify(data));
   updateSaveedGameList();
}

function loadGame(name) {
   // загружаем данные
   const localData = localStorage.getItem('save');
   const allGames = JSON.parse(localData);
   const currrentGame = allGames[name];

   // передаем в state новой матрице загруженные данные
   matrix.state = [];
   matrix.state.push(currrentGame.state);
   matrix.render(dom);
   setStepInfo();
   closeModal();
}

function newGame() {
   matrix = new Matrix(12, 25);
   matrix.render(dom);
   setStepInfo();
   closeModal();
}

function openModal() {
   $('#myModal').modal('show');
}

function closeModal() {
   $('#myModal').modal('hide');
}

function deleteAll() {
   localStorage.clear();
   updateSaveedGameList();
}

function updateSaveedGameList() {
   const listSaveGame = document.getElementById('listSaveGame');
   const allGames = loadAllGames();

   listSaveGame.innerHTML = '';

   for (let name in allGames) {
      const button = document.createElement('button');

      button.onclick = () => loadGame(name);
      button.innerText = `${name}  Steps:${allGames[name].step}`;
      listSaveGame.append(button);
   }

   function loadAllGames() {
      const data = localStorage.getItem('save');
      return JSON.parse(data);
   }
}
