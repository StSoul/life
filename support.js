const sup = (() => ({
    random: (min, max) => {
        let rand = min + Math.random() * (max + 1 - min);
        return Math.floor(rand);
    },
    getWithProbability: (probability) => {
        if (probability === 0) return 0;
        if (probability === 100) return 1;

        const r = sup.random(0, 100);
        return r <= probability ? 1 : 0;
    },
    printArr: (arr) => {
        for (let i = 0; i < arr.length; i++) {
            console.log(...arr[i]);
        }
    },
    clone: (arr) => {
        const res = [];

        for (let i = 0; i < arr.length; i++) {
            const row = [];

            for (let j = 0; j < arr[i].length; j++) {
                row.push(arr[i][j]);
            }
            res.push(row);
        }

        return res;
    }
}))();