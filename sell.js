const Cell = (() => ({
   getCountCellsAround: (point, matrix) => {
      let count = 0;
      const [rowIndex, elemIndex] = point;
      const currentRow = matrix[rowIndex];
      const previousRow = matrix[rowIndex - 1];
      const nextRow = matrix[rowIndex + 1];

      // наша позиция всегда № 5
      // 1 2 3
      // 4 5 6
      // 7 8 9

      // проверяем верхний ряд (номера 1, 2, 3)
      if (previousRow) {
         // 1
         if (previousRow[elemIndex - 1] === 1) count++;
         // 2
         if (previousRow[elemIndex] === 1) count++;
         // 3
         if (previousRow[elemIndex + 1] === 1) count++;
      }

      // проверяем нижний ряд (номера 7, 8, 9)
      if (nextRow) {
         // 7
         if (nextRow[elemIndex - 1] === 1) count++;
         // 8
         if (nextRow[elemIndex] === 1) count++;
         // 9
         if (nextRow[elemIndex + 1] === 1) count++;
      }

      // проверям позицию № 4 и 6
      if (currentRow[elemIndex - 1] === 1) count++;
      if (currentRow[elemIndex + 1] === 1) count++;

      return count;
   },
   getStatus: (cell, sellAround) => {
      // если это пустая ячейка и вокруг нее 3 живые клетки, то:
      if (cell === 0 && sellAround === 3) return 1;

      // если клетка живая (равна 1), то проверяем на кол-во соседей
      if (sellAround > 3 || sellAround < 2) return 0;

      return cell;
   }
}))();
